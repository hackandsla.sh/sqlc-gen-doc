package main_test

import (
	"bytes"
	"flag"
	"fmt"
	"io"
	"io/fs"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"testing"

	"gitlab.com/hackandsla.sh/sqlc-gen-doc/internal/rcopy"

	"github.com/sergi/go-diff/diffmatchpatch"
)

var update = flag.Bool("update", false, "update golden files")

func TestMain(m *testing.M) {
	if err := compilePlugin(); err != nil {
		log.Printf("%v\n", err)
		os.Exit(1)
	}

	if _, err := exec.LookPath("sqlc"); err != nil {
		log.Printf("couldn't find sqlc: %v\n", err)
		os.Exit(1)
	}

	os.Exit(m.Run())
}

func Test_Hello(t *testing.T) {
	// Arrange
	testDir := "testdata/table_with_index"
	tmpDir := t.TempDir()
	rcopy.CopyDirectory(testDir, tmpDir)

	goldenFiles := filepath.Join(testDir, "golden")
	gotFiles := filepath.Join(tmpDir, "golden")
	if err := os.RemoveAll(gotFiles); err != nil {
		t.Fatalf("error while cleaning directory '%s': %v", goldenFiles, err)
	}

	cmd := exec.Command("sqlc", "generate", ".")
	cmd.Dir = tmpDir
	var stderr bytes.Buffer
	cmd.Stderr = &stderr

	// Act
	err := cmd.Run()
	if err != nil {
		t.Fatalf("expected no error code, got %v\n\n%s", err, stderr.String())
	}

	// Update golden files
	if *update {
		if err := os.RemoveAll(goldenFiles); err != nil {
			t.Fatalf("error while removing current golden files '%s': %v", goldenFiles, err)
		}

		if err := os.MkdirAll(goldenFiles, 0777); err != nil {
			t.Fatalf("error while creating golden files dir '%s': %v", goldenFiles, err)
		}

		if err := rcopy.CopyDirectory(gotFiles, goldenFiles); err != nil {
			t.Fatalf("error while updating golden files: %v", err)
		}
	}

	// Assert
	filepath.WalkDir("testdata/table_with_index/golden", func(wantFile string, d fs.DirEntry, err error) error {
		gotFileBase, err := filepath.Rel("testdata/table_with_index", wantFile)
		if err != nil {
			t.Fatalf("couldn't compute rel path: %v", err)
			return nil
		}

		gotFile := filepath.Join(cmd.Dir, gotFileBase)
		f, err := os.Open(gotFile)
		if err != nil {
			t.Errorf("expected file '%v' to exist, but it doesn't", gotFileBase)
			return nil
		}

		defer f.Close()

		if d.IsDir() {
			return nil
		}

		gotFileContents, err := io.ReadAll(f)
		if err != nil {
			t.Errorf("couldn't read file '%s'", gotFile)
		}

		wantFileContents, err := os.ReadFile(wantFile)
		if err != nil {
			t.Errorf("couldn't read file '%s'", wantFile)
		}

		if !bytes.Equal(gotFileContents, wantFileContents) {
			dmp := diffmatchpatch.New()
			diffs := dmp.DiffMain(string(wantFileContents), string(gotFileContents), false)
			t.Errorf("expected equal file contents, but got diff for file '%s':\n\n%s", wantFile, dmp.DiffPrettyText(diffs))

			return nil
		}

		return nil
	})
}

func compilePlugin() error {
	cmd := exec.Command("go", "install", ".")

	if err := cmd.Run(); err != nil {
		return fmt.Errorf("couldn't compile sqlc-gen-doc plugin: %w", err)
	}

	return nil
}
