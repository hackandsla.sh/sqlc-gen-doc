CREATE TABLE authors (
  id BIGSERIAL PRIMARY KEY,
  name text NOT NULL,
  bio text
);

CREATE UNIQUE INDEX name_idx ON authors(name);