module gitlab.com/hackandsla.sh/sqlc-gen-doc

go 1.19

require (
	github.com/sergi/go-diff v1.2.0
	google.golang.org/protobuf v1.28.1
)
